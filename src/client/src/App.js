import React from 'react';
import {
	BrowserRouter,
	Route,
	Switch,
	Redirect,
} from 'react-router-dom';
import Register from './components/Register';
import Login from './components/Login';
import Dashboard from './components/Dashboard';
import AddNewTask from './components/AddNewTask';

const App = () => (
	<BrowserRouter>
		<div className="App">
			<Switch>
				<Route exact path="/" render={() => <Redirect to="/dashboard" />} />
				<Route path="/dashboard/AddNewTask" component={AddNewTask} />
				<Route path="/dashboard" component={Dashboard} />
				<Route path="/register" component={Register} />
				<Route path="/login" component={Login} />
			</Switch>
		</div>
	</BrowserRouter>
);

export default App;
