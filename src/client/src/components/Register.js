import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';

class Register extends Component {
	state = {
		message: '',
	}

	handleSubmit = (e) => {
		e.preventDefault();
		const form = e.target;
		const data = {};
		for (let i = 0; i < form.length - 1; i += 1) {
			data[form[i].id] = form[i].value;
		}
		if (data.password === data.passwordRepeat) {
			delete data.passwordRepeat;
			fetch('http://localhost:5000/api/register', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					Accept: 'application/json',
				},
				body: JSON.stringify(data),
			}).then((res) => {
				if (res.status === 200) {
					const { props } = this;
					this.setState({ message: 'Registered Successfully!' });
					form.reset();
					setTimeout(() => {
						props.history.push('/Login/');
					}, 2000);
				} else if (res.status === 400) {
					this.setState({ message: res.statusText });
				}
			});
		} else this.setState({ message: 'Passwords do not match.' });
	}

	render() {
		const { state, handleSubmit } = this;
		/* jshint ignore:start */
		/* eslint-disable */
		return (
			<div>
				<div className="row" id="container">
					<form className="col s6" id="form" onSubmit={handleSubmit}>
						<div className="row">
							<NavLink to="/Login" className="waves-effect waves-light btn-large btnSwitchForm">Login
								<i className="material-icons right">vpn_key</i>
							</NavLink>
							<button type="submit" className="waves-effect waves-light btn-large btnCurrentForm btnSwitchForm">Register
								<i className="material-icons right">person_add</i>
							</button>
						</div>
						<div className="row">
							<div className="input-field col s6">
								<input id="first_name" type="text" className="validate" required="required" aria-required="true" />
								<label htmlFor="first_name">First Name</label>
							</div>
							<div className="input-field col s6">
								<input id="last_name" type="text" className="validate" required="required" aria-required="true" />
								<label htmlFor="last_name">Last Name</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s12">
								<input id="username" type="text" className="validate" required="required" aria-required="true" />
								<label htmlFor="username">Username</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s12">
								<input id="password" type="password" className="validate" required="required" aria-required="true" />
								<label htmlFor="password">Password</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s12">
								<input id="passwordRepeat" type="password" className="validate" required="required" aria-required="true" />
								<label htmlFor="passwordRepeat">Repeat Password</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s12">
								<input id="email" type="email" className="validate" required="required" aria-required="true" />
								<label htmlFor="email">Email</label>
							</div>
						</div>
						<button type="submit" className="waves-effect waves-light btn" id="btnSubmit">Submit
							<i className="material-icons right">send</i>
						</button>
					</form>
				</div>
				<p className="addTaskMessage">{state.message}</p>
			</div>
		);
		/* jshint ignore:start */
		/* eslint-disable */
	}
}

export default withRouter(Register);
