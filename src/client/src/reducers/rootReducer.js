const { fromJS } = require('immutable');

const initState = fromJS({
	authorized: false,
	admin: false,
	tasks: [],
});

const rootReducer = (state = initState, action) => {
	if (action.type === 'SET_ADMIN') {
		return state.set('admin', action.admin);
	}
	if (action.type === 'SET_AUTHORIZED') {
		return state.set('authorized', action.authorized);
	}
	if (action.type === 'SET_TASKS') {
		return state.set('tasks', fromJS(action.tasks));
	}
	if (action.type === 'SET_REDUX_STATE') {
		return fromJS(action.newState);
	}
	return state;
};

export default rootReducer;
