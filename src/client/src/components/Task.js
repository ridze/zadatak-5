import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

const Task = ({ data }) => (
	<div className="col s12 m6">
		<div className="card white taskCard">
			<div className="card-content">
				<span className="card-title">{data.get('title')}</span>
				<p>{data.get('description')}</p>
			</div>
			<div className="card-action taskAction">
				<span className="taskStatus">status: </span>
				{(data.get('assignedTo') !== 'available'
					?	(
						<span className="taskStatusText">
							<span>
								<i>assigned to </i>
							</span>
							<span>
								<b>{data.get('assignedTo')}</b>
							</span>
						</span>
					) : (<span className="statusAvailable"><b>Available</b></span>
					))}
			</div>
		</div>
	</div>
);

Task.propTypes = {
	data: ImmutablePropTypes.mapContains({
		title: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		assignedTo: PropTypes.string.isRequired,
		id: PropTypes.number.isRequired,
	}).isRequired,
};

export default Task;
