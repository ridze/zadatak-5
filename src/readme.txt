This is a simple create & assign todos (tasks) app. 
First user that registers (with the db id of 1) has admin privileges - he assigns tasks to other users.
Other users can see their tasks at dashboard.
Every user can see all tasks.

Used wamp server locally for db.


Could not find fix for following eslint errors: 
 
- 'A form label must be associated with a control jsx-a11y/label-has-associated-control'
at: \client\components\AddTaskForm\ 11:6, 15:6, 20:7;
    \client\components\Register\ 57:9, 61:9, 67:9, 73:9, 79:9, 85:9;	

- ' `i` must be placed on a new line' ('i' is placed ona a new line)
at: \client\components\AddTaskForm\ 24:6;

Used /* jshint ignore:start */ /* eslint-disable */ to pass checks.