module.exports = (sequelize, type, bcrypt) => (
	sequelize.define('user', {
		first_name: {
			type: type.STRING,
			allowNull: false,
		},
		last_name: {
			type: type.STRING,
			allowNull: false,
		},
		username: {
			type: type.STRING,
			unique: true,
			allowNull: false,
		},
		password: {
			type: type.STRING,
			unique: true,
			allowNull: false,
		},
		email: {
			type: type.STRING,
			allowNull: false,
		},
	}, {
		hooks: {
			afterValidate: (user) => {
				user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10));
			},
		},
	})
);
