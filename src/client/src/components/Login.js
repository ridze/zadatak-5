import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import axios from 'axios';

class Login extends Component {
	state = {
		message: '',
	}

	handleSubmit = (e) => {
		e.preventDefault();
		const form = e.target;
		const data = {};
		for (let i = 0; i < form.length - 1; i += 1) {
			data[form[i].id] = form[i].value;
		}
		axios({
			method: 'post',
			url: 'http://localhost:5000/api/login',
			data,
			withCredentials: true,
		})
			.then(() => {
				const { props } = this;
				props.history.push('/Dashboard/');
			})
			.catch(() => this.setState({ message: 'Please, enter valid login data' }));
	}

	render() {
		const { state, handleSubmit } = this;
		return (
			<div id="loginWrapper">
				<div className="row" id="container">
					<form className="col s6" id="form" onSubmit={handleSubmit}>
						<div className="row">
							<button type="submit" className="waves-effect waves-light btn-large btnSwitchForm btnCurrentForm">
								Login
								<i className="material-icons right">vpn_key</i>
							</button>
							<NavLink to="/Register" className="waves-effect waves-light btn-large btnSwitchForm">
								Register
								<i className="material-icons right">person_add</i>
							</NavLink>
						</div>
						<div className="row">
							<div className="input-field col s12">
								<input id="username" type="text" className="validate" required="required" aria-required="true" />
								<label htmlFor="username">Username</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s12">
								<input id="password" type="password" className="validate" required="required" aria-required="true" />
								<label htmlFor="password">Password</label>
							</div>
						</div>
						<button type="submit" className="waves-effect waves-light btn" value="submit" id="btnSubmit">Login</button>
					</form>
				</div>
				<p className="addTaskMessage">{state.message}</p>
			</div>
		);
	}
}

export default Login;
