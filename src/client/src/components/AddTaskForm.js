import React from 'react';
import PropTypes from 'prop-types';

const AddTaskForm = ({ onSubmit }) => (
	/* jshint ignore:start */
	/* eslint-disable */
	<div className="row" id="addTaskContainer">
		<form id="addTaskForm" className="col s6" onSubmit={onSubmit}>
			<h4 className="formTitle">Add New Task</h4>
			<div className="row">
				<div className="input-field col s6">
					<input id="taskTitle" type="text" className="validate" />
					<label htmlFor="taskTitle">Title</label> 
				</div>
				<div className="input-field col s6">
					<input id="assignee" type="text" className="validate" />
					<label htmlFor="assignee">Assigned to</label>
				</div>
				<div className="row">
					<div className="input-field col s12">
						<textarea id="taskDescription" className="materialize-textarea" />
						<label htmlFor="taskDescription">Description</label>
					</div>
				</div>
				<button type="submit" className="waves-effect waves-light btn" id="btnSubmit">Submit
					<i className="material-icons right">send</i>
				</button>
			</div>
		</form>
	</div>
	/* jshint ignore:start */
	/* eslint-disable */
);

AddTaskForm.propTypes = {
	onSubmit: PropTypes.func.isRequired,
};

export default AddTaskForm;
