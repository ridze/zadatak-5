import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import Task from './Task';

const Tasks = ({ tasks }) => {
	const myTasks = [];
	const n = tasks.size;
	for (let i = 0; i < n; i += 1) {
		myTasks.push(
			<Task data={tasks.get(i)} key={i} />
		);
	}
	return (
		<div>
			<div className="row">
				{myTasks}
			</div>)
		</div>
	);
};

Tasks.propTypes = {
	tasks: ImmutablePropTypes.listOf(
		ImmutablePropTypes.contains({
			id: PropTypes.number.isRequired,
			title: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			assignedTo: PropTypes.string.isRequired,
		})
	).isRequired,
};

export default Tasks;
