const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const router = require('./api');

const app = express();

app.use(session({
	cookie: {
		path: '/',
		httpOnly: false,
		maxAge: 24 * 60 * 60 * 1000,
	},
	secret: 'work hard',
	resave: true,
	saveUninitialized: false,
}));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((request, response, next) => {
	response.header('Access-Control-Allow-Origin', 'http://localhost:3000');
	response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	response.header('Access-Control-Allow-Credentials', true);
	next();
});

app.use(router);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Listening on port ${port}`));
