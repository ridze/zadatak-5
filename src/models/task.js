module.exports = (sequelize, type) => (
	sequelize.define('task', {
		title: {
			type: type.STRING,
			allowNull: false,
		},
		description: {
			type: type.STRING,
			allowNull: false,
		},
		assignedTo: {
			type: type.STRING,
		},
	})
);
