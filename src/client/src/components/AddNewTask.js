import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import Task from './Task';
import Navbar from './Navbar';
import AddTaskForm from './AddTaskForm';


class AddNewTask extends Component {
	state = {
		message: '',
	}

	componentDidMount() {
		const { props } = this;
		axios({
			method: 'get',
			url: 'http://localhost:5000/api/dashboard',
			withCredentials: true,
		}).then((res) => {
			if (res.data.authorized) {
				props.setReduxState(res.data);
			} else {
				props.history.push('/Login');
			}
		}).catch(() => props.history.push('/Login'));
	}

	handleAddTask = (e) => {
		e.preventDefault();
		const form = e.target;
		const data = {};
		data.title = form[0].value;
		data.assignedTo = form[1].value ? form[1].value : 'available';
		data.description = form[2].value;
		axios({
			method: 'post',
			url: 'http://localhost:5000/api/addTask',
			data,
			withCredentials: true,
		}).then((res) => {
			if (res.status === 200) {
				this.setState({
					message: 'Added Task Successfully.',
				});
				form.reset();
			}
		}).catch((err) => {
			if (err.response.status === 401) {
				this.setState({
					message: err.response.statusText,
				});
			}
		});
	}

	getMyTasks = (e) => {
		e.preventDefault();
		axios({
			method: 'get',
			url: 'http://localhost:5000/api/mytasks',
			withCredentials: true,
		}).then((res) => {
			const { props } = this;
			props.setTasks(res.data.tasks);
		});
	}

	getAllTasks = (e) => {
		e.preventDefault();
		axios({
			method: 'get',
			url: 'http://localhost:5000/api/dashboard',
			withCredentials: true,
		}).then((res) => {
			const { props } = this;
			props.setTasks(res.data.tasks);
		});
	}

	handleLogout = () => {
		axios({
			method: 'get',
			url: 'http://localhost:5000/api/logout',
			withCredentials: true,
		}).then(() => {
			const { props } = this;
			props.setReduxState({ tasks: [], admin: false, authorized: false });
			props.history.push('/Login');
		});
	}

	render() {
		const tasks = [];
		const { props, state } = this;
		const n = props.tasks.size;
		for (let i = 0; i < n; i += 1) tasks.push(<Task data={props.tasks.get(i)} key={i} />);
		return (
			props.authorized
				?	(
					<div id="dashboardContainer">
						<Navbar
							handleLogout={this.handleLogout}
							getAllTasks={this.getAllTasks}
							getAddTask={this.getAddTask}
							getMyTasks={this.getMyTasks}
							admin={props.admin}
						/>
						<AddTaskForm onSubmit={this.handleAddTask} />
						<p className="addTaskMessage">{state.message}</p>
					</div>
				) : (
					<div id="myPreloader">
						<div className="preloader-wrapper big active">
							<div className="spinner-layer spinner-green-only">
								<div className="circle-clipper left">
									<div className="circle" />
								</div>
								<div className="gap-patch">
									<div className="circle" />
								</div>
								<div className="circle-clipper right">
									<div className="circle" />
								</div>
							</div>
						</div>
					</div>
				)
		);
	}
}

const mapStateToProps = state => ({
	tasks: state.get('tasks'),
	authorized: state.get('authorized'),
	admin: state.get('admin'),
	showAddTask: state.get('showAddTask'),
});

const mapDispatchToProps = dispatch => ({
	setTasks: (tasks) => { dispatch({ type: 'SET_TASKS', tasks }); },
	setAuthorized: (authorized) => { dispatch({ type: 'SET_AUTHORIZED', authorized }); },
	setAdmin: (admin) => { dispatch({ type: 'SET_ADMIN', admin }); },
	setReduxState: (newReduxState) => { dispatch({ type: 'SET_REDUX_STATE', newState: newReduxState }); },
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddNewTask));
