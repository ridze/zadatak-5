import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

class Navbar extends Component {
	constructor(...args) {
		super(...args);
		const { handleLogout, getAllTasks, getMyTasks } = this.props;
		this.state = {
			handleLogout,
			getAllTasks,
			getMyTasks,
		};
	}

	render() {
		const { state, props } = this;
		/* jshint ignore:start */
		/* eslint-disable */
		return (
			<nav id="dashboardNav">
				<div className="nav-wrapper  teal lighten-1" id="navMain">
					<button type="submit" className="brand-logo right" id="logout" onClick={state.handleLogout}>Logout</button>
					{(props.admin
						?	(
							<ul id="nav-mobile" className="left">
								<li>
									<NavLink exact to="/Dashboard">
										<i className="material-icons right">filter_none</i>
										All Tasks
									</NavLink>
								</li>
								<li>
									<NavLink exact to="/Dashboard/AddNewTask">
										<i className="material-icons right">add</i>
										Add Task
									</NavLink>
								</li>
							</ul>
						) : (
							<ul id="nav-mobile" className="left leftNav">
								<li onKeyUp={state.getMyTasks} onClick={state.getMyTasks} className="leftNav">
									<i className="material-icons right">filter_frames</i>
									My Tasks
								</li>
								<li onKeyUp={state.getMyTasks} onClick={state.getAllTasks}>
									<i className="material-icons right">filter_none</i>
									All Tasks
								</li>
							</ul>)
					)}
				</div>
			</nav>
		);
		/* eslint-enable */
		/* jshint ignore:end */
	}
}

Navbar.propTypes = {
	handleLogout: PropTypes.func.isRequired,
	getMyTasks: PropTypes.func.isRequired,
	getAllTasks: PropTypes.func.isRequired,
};

export default Navbar;
