const express = require('express');
const bcrypt = require('bcrypt-nodejs');
const { User, Task } = require('./sequelize');

const router = express.Router();

router.post('/api/login', (req, res) => {
	const client = req.body;
	User.findOne({ where: { username: client.username } })
		.then((user) => {
			if (user) {
				bcrypt.compare(client.password, user.password, (err, result) => {
					if (err) {
						res.status(400);
						res.statusMessage = 'wrong password';
						res.end();
					} else if (result) {
						req.session.userId = user.id;
						req.session.username = user.username;
						res.end();
					} else {
						res.status(400);
						res.statusMessage = 'wrong password';
						res.end();
					}
				});
			} else {
				res.status(400);
				res.statusMessage = 'wrong username';
				res.end();
			}
		});
});

router.post('/api/register', (req, res) => {
	User.create(req.body)
		.then((result) => {
			res.send(result);
		})
		.catch((err) => {
			res.status(400);
			res.statusMessage = err.errors[0] ? err.errors[0].message : 'Could not register';
			res.end();
		});
});

router.post('/api/addTask', (req, res) => {
	if (req.session.userId === 1) {
		User.findOne({ where: { username: req.body.assignedTo } })
			.then((result) => {
				if (result) {
					Task.create(req.body).then(() => {
						res.statusMessage = 'Created new task.';
						res.end();
					}).catch(() => {
						res.status(400);
						res.end();
					});
				} else {
					res.statusMessage = 'wrong assignee username';
					res.status(401).end();
				}
			}).catch(() => {
				res.status(400);
				res.end();
			});
	} else {
		res.statusMessage = 'You do not have admin permission.';
		res.end();
	}
});

router.get('/api/dashboard', (req, res) => {
	if (req.session && req.session.userId) {
		const allTasks = [];
		Task.findAll({}).then((tasks) => {
			for (let i = 0; i < tasks.length; i += 1) allTasks.push(tasks[i].dataValues);
			res.send((req.session.userId === 1)
				? { authorized: true, admin: true, tasks: allTasks }
				: { authorized: true, admin: false, tasks: allTasks });
		});
	} else {
		res.status(400);
		res.send({ authorized: false });
	}
});

router.get('/api/mytasks', (req, res) => {
	if (req.session && req.session.userId) {
		User.findOne({ where: { username: req.session.username } }).then((client) => {
			const myTasks = [];
			Task.findAll({ where: { assignedTo: client.username } }).then((tasks) => {
				for (let i = 0; i < tasks.length; i += 1) myTasks.push(tasks[i].dataValues);
				res.send({ tasks: myTasks });
			});
		});
	} else {
		res.send({ authorized: false });
	}
});

router.get('/api/logout', (req, res) => {
	if (req.session) {
		req.session.destroy((err) => {
			if (err) {
				res.send(err);
			} else {
				res.send({ loggedOut: true });
			}
		});
	}
});

module.exports = router;
