const Sequelize = require('sequelize');
const bcrypt = require('bcrypt-nodejs');
const UserModel = require('./models/user');
const TaskModel = require('./models/task');

const connection = new Sequelize('demo_schema', 'root', '', {
	dialect: 'mysql',
});

const User = UserModel(connection, Sequelize, bcrypt);
const Task = TaskModel(connection, Sequelize);

connection
	.sync({
		force: false,
		logging: console.log,
	})
	.catch((error) => {
		console.log(error);
	});

module.exports = {
	User,
	Task,
};
